import uuid

import pytest

import piperci_gman.gman as gman

gman_put_statuses = [
    ('started', 422),
    ('completed', 200),
    ('failed', 200),
    ('delegated', 200),
    ('received', 422),
    ('info', 200)
]


@pytest.mark.parametrize('status,resp_code', gman_put_statuses)
def test_put_statuses(status, resp_code, api, client, testtask):
    task_id = testtask().json['task']['task_id']
    event = {
        'status': status,
        'message': f'Testing with status={status}'
    }

    resp = client.put(api.url_for(gman.TaskResource, task_id=task_id), json=event)
    assert resp.status_code == resp_code, f'Invalid response code {resp.status_code}'


def test_put_completed_twice(api, client, testtask):
    task_id = testtask().json['task']['task_id']
    event = {
        'status': 'completed',
        'message': f'Testing with status=completed'
    }

    resp = client.put(api.url_for(gman.TaskResource, task_id=task_id), json=event)
    assert resp.status_code == 200, f'Invalid response code {resp.status_code}'

    resp = client.put(api.url_for(gman.TaskResource, task_id=task_id), json=event)
    assert resp.status_code == 422, f'Invalid response code {resp.status_code}'


def test_put_accessed_artifact(api, client, testtask, artifact):

    task_id = testtask().json['task']['task_id']
    art_id = artifact()

    event = {
        'status': 'info',
        'message': f'Testing with status=info and an artifact',
        'artifact': f'{art_id[0]["artifact_id"]}'
    }

    resp = client.put(api.url_for(gman.TaskResource, task_id=task_id), json=event)

    assert resp.status_code == 200, 'Failed to create an event that uses an artifact'


def test_put_bad_body(api, client, testtask):
    task_id = testtask().json['task']['task_id']
    event = {
        'status': 'asdfasdfsd',
        'message': f'Testing with status info bad body',
        'caller': f'test put info bad body',
        'thread_id': 'some_id'
    }

    resp = client.put(api.url_for(gman.TaskResource, task_id=task_id), json=event)
    assert resp.status_code == 422, f'Invalid response code {resp.status_code}'


def test_put_bad_task_id(api, client):

    task_id = uuid.uuid4()
    event = {
        'status': 'info',
        'message': f'Testing with status info with /events',
    }

    resp = client.put(api.url_for(gman.TaskResource, task_id=task_id),
                      json=event)
    assert resp.status_code == 404, f'Expected 404 but got {resp.status_code}'


def test_put_w_events(api, client, testtask):

    task_id = testtask().json['task']['task_id']
    event = {
        'status': 'info',
        'message': f'Testing with status info with /events',
    }

    resp = client.put(api.url_for(gman.TaskResource, task_id=task_id, events='events'),
                      json=event)
    assert resp.status_code == 400, f'Expected 400 but got {resp.status_code}'
