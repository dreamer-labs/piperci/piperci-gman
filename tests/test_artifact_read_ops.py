import pytest

import piperci_gman.gman as gman
import piperci_gman.artman as artman


def test_get_artifact(api, client, artifacts):

    for artifact in artifacts:
        art_id = artifact[0].json['artifact_id']
        resp = client.get(f'/artifact/{art_id}')
        assert resp.status_code == 200


def test_get_artifact_bad_request(api, client):
    resp = client.get('/artifact')
    assert resp.status_code == 400


def test_get_bad_artifact(api, client, artifacts):
    resp = client.get(f'/artifact/31a122e8-9ba8-4f60-a9fb-490c66fd4b0a')
    assert resp.status_code == 404


def test_get_artifacts_by_task_id(api, client, artifact):
    task_id = artifact()[0]['task']['task_id']

    resp = client.get(api.url_for(artman.ArtifactResource, task_id=task_id))
    assert len(resp.json) == 1
    assert resp.json[0]['task']['task_id'] == task_id


def test_get_artifacts_by_bad_task_id(api, client, artifact):
    artifact()
    task_id = '31a122e8-9ba8-4f60-a9fb-490c66fd4b0a'
    resp = client.get(api.url_for(artman.ArtifactResource, task_id=task_id))
    assert resp.status_code == 404


def test_get_artifact_by_bad_sri(api, client, artifact):
    artifact()
    bad_sri = 'c2hhMjU2LXZGYXRjZXlXYUU5QWtzM045b3VSVXRiYTFtd3JJSGRFVkx0aTg4YXRJdmM9'
    resp = client.get(f'/artifact/sri/{bad_sri}')
    assert resp.status_code == 404


def test_get_artifacts_by_sri(api, client, artifacts):
    resp = client.get(f'/artifact/sri/{artifacts[0][1]["sri-urlsafe"]}')
    assert resp.status_code == 200
    assert len(resp.json) == 2


def test_get_artifacts_by_thread_id(api, client, artifacts):
    artifact_resp = artifacts[0][1]
    task_resp = client.get(api.url_for(gman.TaskResource,
                                       task_id=artifact_resp['task_id']))
    resp = client.get(f'/artifact/thread/{task_resp.json["thread_id"]}')
    assert resp.status_code == 200
    assert len(resp.json) == 2


def test_get_artifacts_by_run_id(api, client, artifacts):
    artifact_resp = artifacts[0][1]
    task_resp = client.get(api.url_for(gman.TaskResource,
                                       task_id=artifact_resp['task_id']))
    resp = client.get(f'/artifact/run/{task_resp.json["run_id"]}')
    assert resp.status_code == 200
    assert len(resp.json) == 3


def test_get_artifacts_bad_thread_id(api, client, artifacts):
    resp = client.get(f'/artifact/thread/31a122e8-9ba8-4f60-a9fb-490c66fd4b0a')
    assert resp.status_code == 404


def test_get_artifacts_bad_run_id(client, artifacts):
    resp = client.get(f'/artifact/run/1234')
    assert resp.status_code == 404


@pytest.fixture
def event_with_artifact(api, client, artifact, testtask):
    task_id = testtask().json['task']['task_id']
    art_id = artifact()

    event = {
        'status': 'info',
        'message': f'Testing with status=info and an artifact',
        'artifact': f'{art_id[0]["artifact_id"]}'
    }

    resp = client.put(api.url_for(gman.TaskResource, task_id=task_id), json=event)

    assert resp.status_code == 200
    return resp.json


def test_get_artifacts_accessed_task_id(api, client, event_with_artifact, artifact):

    task_id = event_with_artifact['task']['task_id']

    art2 = artifact(data={'task_id': task_id})

    resp = client.get(f'/artifact/task/{task_id}?filter=accessed_by')
    resp2 = client.get(f'/artifact/task/{task_id}?filter=accessed_by,created_by')

    assert resp.status_code == 200
    assert resp.json[0]['artifact_id'] == event_with_artifact['artifact']

    assert resp2.status_code == 200
    assert (set((art2[0]['artifact_id'], event_with_artifact['artifact']))
            == set([x['artifact_id'] for x in resp2.json]))


def test_get_artifacts_accessed_thread_id(api, client, event_with_artifact, artifact):

    task_id = event_with_artifact['task']['task_id']
    art2 = artifact(data={'task_id': task_id})

    resp = client.get(f'/artifact/thread/{task_id}?filter=accessed_by')
    resp2 = client.get(f'/artifact/thread/{task_id}'
                       '?filter=accessed_by,created_by')

    assert resp.status_code == 200
    assert resp.json[0]['artifact_id'] == event_with_artifact['artifact']

    assert resp2.status_code == 200
    assert (set((art2[0]['artifact_id'], event_with_artifact['artifact']))
            == set([x['artifact_id'] for x in resp2.json]))


def test_get_artifacts_accessed_run_id(api, client, event_with_artifact, artifact):

    run_id = event_with_artifact['task']['run_id']
    art2 = artifact(data={'task_id': event_with_artifact['task']['task_id']})

    resp = client.get(f'/artifact/run/{run_id}?filter=accessed_by')
    resp2 = client.get(f'/artifact/run/{run_id}?filter=accessed_by,created_by')

    assert resp.status_code == 200
    assert resp.json[0]['artifact_id'] == event_with_artifact['artifact']

    assert resp2.status_code == 200
    assert (set((art2[0]['artifact_id'], event_with_artifact['artifact']))
            == set([x['artifact_id'] for x in resp2.json]))


def test_head_artifacts_accessed_task_id(api, client, event_with_artifact):

    task_id = event_with_artifact['task']['task_id']
    resp = client.head(f'/artifact/task/{task_id}?filter=accessed_by')

    assert resp.status_code == 200
    assert int(resp.headers['x-gman-artifacts']) == 1


def test_head_artifacts_accessed_thread_id(api, client, event_with_artifact):

    task_id = event_with_artifact['task']['task_id']
    resp = client.head(f'/artifact/thread/{task_id}?filter=accessed_by')

    assert resp.status_code == 200
    assert int(resp.headers['x-gman-artifacts']) == 1


def test_head_artifacts_accessed_run_id(api, client, event_with_artifact):

    run_id = event_with_artifact['task']['run_id']
    resp = client.head(f'/artifact/run/{run_id}?filter=accessed_by')

    assert resp.status_code == 200
    assert int(resp.headers['x-gman-artifacts']) == 1


def test_head_artifact_bad_request(api, client):
    resp = client.head('/artifact')
    assert resp.status_code == 400


def test_head_artifact(api, client, artifacts):

    for artifact in artifacts:
        art_id = artifact[0].json['artifact_id']
        resp = client.head(f'/artifact/{art_id}')
        assert resp.status_code == 200
        assert resp.headers['x-gman-artifact-status'] == 'unknown'


def test_head_bad_artifact(api, client, artifacts):

    resp = client.head(f'/artifact/31a122e8-9ba8-4f60-a9fb-490c66fd4b0a')
    assert resp.status_code == 404


def test_head_artifacts_by_sri(api, client, artifacts):
    resp = client.head(f'/artifact/sri/{artifacts[0][1]["sri-urlsafe"]}')
    assert resp.status_code == 200
    assert int(resp.headers['x-gman-artifacts']) == 2


def test_head_artifacts_by_thread_id(api, client, artifacts):
    artifact_resp = artifacts[0][1]
    task_resp = client.get(api.url_for(gman.TaskResource,
                                       task_id=artifact_resp['task_id']))
    resp = client.head(f'/artifact/thread/{task_resp.json["thread_id"]}')
    assert resp.status_code == 200
    assert int(resp.headers['x-gman-artifacts']) == 2


def test_head_artifacts_by_run_id(api, client, artifacts):
    artifact_resp = artifacts[0][1]
    task_resp = client.get(api.url_for(gman.TaskResource,
                                       task_id=artifact_resp['task_id']))
    resp = client.head(f'/artifact/run/{task_resp.json["run_id"]}')
    assert resp.status_code == 200
    assert int(resp.headers['x-gman-artifacts']) == 3


def test_head_artifacts_for_task_id(api, client, artifact):
    task_id = artifact()[0]['task']['task_id']

    resp = client.head(api.url_for(artman.ArtifactResource, task_id=task_id))
    assert int(resp.headers['x-gman-artifacts']) == 1
