from piperci_gman.orm.models import Artifact


def test_good_health(client):

    resp = client.get('/health')

    assert resp.status_code == 200


def test_bad_health(client, db):

    db.drop_tables([Artifact])

    resp = client.get('/health')

    assert resp.status_code == 500
