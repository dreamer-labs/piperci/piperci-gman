import datetime

import pytest

import piperci_gman.artman as artman

from piperci_gman.orm.models import Artifact, Task, TaskEvent, db


def test_post_artifact_no_task(api, client, artifacts_data):
    art = {'task_id': '7d394a53-6f45-4847-bfd1-105eef07dd08'}

    art.update(artifacts_data[0])
    resp = client.post(api.url_for(artman.ArtifactResource), json=art)

    assert resp.status_code == 404, 'Code failed to check that the task exists'
    assert 'errors' in resp.json, 'Missing expected errors response'
    assert 'task_id' in resp.json['errors'], (
        'Did not throw the correct error for this test')


def test_post_bad_artifact_url(api, client):

    resp = client.post('/artifact/31a122e8-9ba8-4f60-a9fb-490c66fd4b0a')
    assert resp.status_code == 400


def test_post_same_artifact_twice(api, client, artifact, artifacts_data):

    art = {'task_id': artifact()[0]['task']['task_id']}
    art.update(artifacts_data[0])

    resp = client.post(api.url_for(artman.ArtifactResource), json=art)

    assert resp.status_code == 409


@pytest.mark.parametrize('dissallowed', ('artifact_id', 'timestamp',
                                         'status', 'event_id'))
def test_post_dissallowed_field(api, client, dissallowed, artifacts_data):

    art = artifacts_data[0].copy()
    art[dissallowed] = 'Some value'
    resp = client.post(api.url_for(artman.ArtifactResource), json=art)
    assert resp.status_code == 422


def test_post_field_value_errors(api, client, artifacts_data):

    art = artifacts_data[0].copy()
    art['type'] = 'asdfasdfs'
    art['task_id'] = 1234
    resp = client.post(api.url_for(artman.ArtifactResource), json=art)
    assert resp.status_code == 422


def test_raw_artifact_bad_hash(testtask):
    task_resp = testtask()
    task = Task().get(Task.task_id == task_resp.json['task']['task_id'])

    event = TaskEvent.create(task=task,
                             message='testing creating an artifact',
                             status='info',
                             timestamp=datetime.datetime.now())
    with pytest.raises(ValueError):
        Artifact().create(
            task=task,
            event_id=event,
            type='log',
            status='unknown',
            sri='some non sri value',
            uri='https://www.example.com'
        )


def test_failed_artifact_create_no_table(api, client, monkeypatch, testtask,
                                         artifacts_data):
    task = testtask()

    db.drop_tables([Artifact])

    art = {'task_id': task.json['task']['task_id']}
    art.update(artifacts_data[0])

    resp = client.post(api.url_for(artman.ArtifactResource), json=art)
    assert resp.status_code == 500


def test_failed_artifact_create_IDK(api, client, monkeypatch, testtask, artifacts_data):
    task = testtask()

    def myfunc(*args, **kwargs):
        kwargs['uri'] = {'not a valid thing'}
        return None

    monkeypatch.setattr('piperci_gman.orm.models.Artifact.create', myfunc)

    art = {'task_id': task.json['task']['task_id']}
    art.update(artifacts_data[0])

    resp = client.post(api.url_for(artman.ArtifactResource), json=art)
    assert resp.status_code == 500
