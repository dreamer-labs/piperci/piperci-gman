import pytest


from piperci_gman.marshaller import Errors


def test_errors_extend_is_dict():

    errors = Errors()

    with pytest.raises(AssertionError):
        errors.extend('something')


def test_errors_extend():

    errors = Errors()

    errors.extend({'x': ['this is a test error']})
    errors.extend({'x': ['another test']})

    assert 'this is a test error' in errors.errors['x']
    assert 'another test' in errors.errors['x']
