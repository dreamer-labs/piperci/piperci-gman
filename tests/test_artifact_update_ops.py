
import piperci_gman.artman as artman


def test_put_artifact(api, client):

    resp = client.put(api.url_for(artman.ArtifactResource))

    assert resp.status_code == 405
