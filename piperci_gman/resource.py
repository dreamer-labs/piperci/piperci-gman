from collections_extended import frozenbag
from flask_restful import Resource
from piperci_gman.orm.models import Artifact, Task, TaskEvent, ZeroResults


class PiperCiResource(Resource):

    def BadRequest(self, message=None):
        message = f': {message}' if message else ''
        return ({'message':
                 f'Bad Request - Could not execute based on sent params{message}'},
                400)

    def NotFound(self, message=None):
        message = f': {message}' if message else ''
        return ({'message': 'Not Found{message}'}, 404)

    def InternalError(self, message=None):
        message = f': {message}' if message else ''
        return ({'message': f'Internal Server Error{message}'}, 500)

    def task_states(self, events):
        """Parses task events to identify which state the task is in."""

        event_buckets = {state: [] for state in ('started', 'received',
                                                 'completed', 'failed',
                                                 'delegated', 'info')}

        for event in events:
            event_buckets[event.status].append(event.task)

        states = {'running': [],
                  'completed': [],
                  'failed': [],
                  'pending': []}

        states['running'] = list((set(event_buckets['started']
                                      + event_buckets['received'])
                                  - set(event_buckets['completed']
                                        + event_buckets['failed'])))

        states['completed'] = list(set(event_buckets['completed']))
        states['failed'] = list(set(event_buckets['failed']))

        pending_ids = list(frozenbag([x.task_id
                                      for x in event_buckets['delegated']])
                           - frozenbag([x.parent_id
                                        for x in event_buckets['received']]))

        delegated_dict = {task.task_id: task
                          for task in event_buckets['delegated']}
        states['pending'] = [delegated_dict[task_id] for task_id in pending_ids]

        return states

    def task(self, task_id):
        return Task.get(Task.task_id == task_id)

    def task_events(self, task_id):
        events = [x for x in
                  TaskEvent.select()
                           .join(Task)
                           .where(Task.task_id == task_id)
                           .order_by(TaskEvent.timestamp)]
        if len(events):
            return events
        else:
            raise ZeroResults(f'No results returned for task_id: {task_id}')

    def task_thread(self, thread_id):
        return {event.task for event in self.task_event_thread(thread_id)}

    def task_event_thread(self, thread_id):
        events = [x for x in
                  TaskEvent.select()
                           .join(Task)
                           .where(Task.thread_id == thread_id)
                           .order_by(TaskEvent.timestamp)]
        if len(events):
            return events
        else:
            raise ZeroResults(f'No results returned for thread_id: {thread_id}')

    def tasks_run_id(self, run_id):
        tasks = [x for x in Task().select().where(Task.run_id == run_id)]
        if len(tasks):
            return tasks
        else:
            raise ZeroResults(f'No results returned for run_id: {run_id}')

    def task_events_run_id(self, run_id):
        events = [x for x in TaskEvent().select()
                                        .join(Task)
                                        .where(Task.run_id == run_id)
                                        .order_by(TaskEvent.timestamp)]
        if len(events):
            return events
        else:
            raise ZeroResults(f'No results returned for run_id: {run_id}')

    def artifacts_by_task_id(self, task_id, filter_by):
        arts = []

        if filter_by.accessed_by:
            arts.extend([x for x in Artifact.select()
                         .distinct()
                         .join(TaskEvent,
                               on=(TaskEvent.artifact == Artifact.artifact_id))
                         .where(TaskEvent.task_id == task_id)
                         .order_by(TaskEvent.timestamp)])

        if filter_by.created_by or not filter_by.bitwise():

            arts.extend([x for x in Artifact.select()
                         .distinct()
                         .join(TaskEvent, on=(TaskEvent.task_id == Artifact.task_id))
                         .where(Artifact.task_id == task_id)
                         .order_by(TaskEvent.timestamp)])

        arts = list(set(arts))

        if len(arts):
            return arts
        else:
            raise ZeroResults(f'No results returned for task_id: {task_id}')

    def artifacts_by_thread_id(self, thread_id, filter_by):
        arts = []

        if filter_by.accessed_by:
            arts.extend([x for x in Artifact.select()
                         .distinct()
                         .join(Task, on=(Task.task_id == TaskEvent.task))
                         .join(TaskEvent,
                               on=(TaskEvent.artifact == Artifact.artifact_id))
                         .where(Task.thread_id == thread_id)
                         .order_by(TaskEvent.timestamp)])

        if filter_by.created_by or not filter_by.bitwise():

            arts.extend([x for x in Artifact.select()
                         .distinct()
                         .join(Task, on=(Task.task_id == Artifact.task_id))
                         .join(TaskEvent, on=(TaskEvent.task_id == Task.task_id))
                         .where(Task.thread_id == thread_id)
                         .order_by(TaskEvent.timestamp)])

        arts = list(set(arts))

        if len(arts):
            return arts
        else:
            raise ZeroResults(f'No results returned for thread_id: {thread_id}')

    def artifacts_by_run_id(self, run_id, filter_by):
        arts = []

        if filter_by.accessed_by:
            arts.extend([x for x in Artifact.select()
                         .distinct()
                         .join(Task, on=(Task.task_id == TaskEvent.task))
                         .join(TaskEvent,
                               on=(TaskEvent.artifact == Artifact.artifact_id))
                         .where(Task.run_id == run_id)
                         .order_by(TaskEvent.timestamp)])

        if filter_by.created_by or not filter_by.bitwise():

            arts.extend([x for x in Artifact.select()
                         .distinct()
                         .join(Task, on=(Task.task_id == Artifact.task_id))
                         .join(TaskEvent, on=(TaskEvent.task_id == Task.task_id))
                         .where(Task.run_id == run_id)
                         .order_by(TaskEvent.timestamp)])

        arts = list(set(arts))

        if len(arts):
            return arts
        else:
            raise ZeroResults(f'No results returned for run_id: {run_id}')

    def artifacts_by_sri(self, sri, validate=False):
        arts = [x for x in Artifact.select()
                                   .join(TaskEvent)
                                   .distinct()
                                   .where((Artifact.sri == sri)
                                          & (TaskEvent.event_id == Artifact.event_id))
                                   .order_by(TaskEvent.timestamp)]
        if len(arts):
            return arts
        else:
            raise ZeroResults(f'No results returned for sri: {sri}')

    def task_completed_event(self, task_id):
        events = [x for x in
                  (TaskEvent.select()
                            .join(Task)
                            .where(
                                (Task.task_id == task_id)
                                & (TaskEvent.status.in_(['failed', 'completed']))))]

        if len(events):
            return events[0]
        else:
            raise ZeroResults(f'No results returned for task_id: {task_id}')
