import datetime
import uuid

from peewee import OperationalError
from piperci_gman.orm.models import (Artifact, Task, TaskEvent,
                                     QueryFailed)
from piperci_gman.resource import PiperCiResource


from marshmallow import EXCLUDE, Schema, fields


class RespSchema(Schema):

    status = fields.Str()
    timestamp = fields.DateTime()
    tasks = fields.Integer()
    task_events = fields.Integer()
    artifacts = fields.Integer()
    request_id = fields.UUID()

    class Meta:
        unknown = EXCLUDE


class HealthCheckResource(PiperCiResource):

    def get(self):
        try:
            return RespSchema().dump({'status': 'Healthy',
                                      'tasks': Task.select().count(),
                                      'task_events': TaskEvent.select().count(),
                                      'artifacts': Artifact.select().count(),
                                      'request_id': uuid.uuid4(),
                                      'timestamp': datetime.datetime.now()})

        except (QueryFailed, OperationalError) as e:
            resp = RespSchema().dump({'status': 'Error: ' + str(e),
                                      'request_id': uuid.uuid4(),
                                      'timestamp': datetime.datetime.now()
                                      })
            return resp, 500
