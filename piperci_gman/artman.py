
import piperci_gman.gman as gman

from flask import request
from peewee import DoesNotExist
from piperci_gman.marshaller import Errors, MarshalError, Marshaller
from piperci_gman.orm.models import (Artifact, ArtifactSchema, QueryFailed,
                                     Task, TaskEvent, TaskSchema, ZeroResults)
from piperci_gman.resource import PiperCiResource
from piperci_gman.util import Flagz
from werkzeug.exceptions import BadRequest
from marshmallow import ValidationError


class ArtManMarshaller(Marshaller):

    def __init__(self, raw):
        super(ArtManMarshaller, self).__init__(raw)

        self._artifact = None
        self._task = None

    def enforce(self, context=None):

        disallowed = ('artifact_id', 'timestamp', 'status', 'event_id')

        required = ('task_id',)

        for key in disallowed:
            if key in self.raw_data:
                self.errors.add(key, f'May not be specified for {context}',
                                data=self.raw_data)

        for key in required:
            if key not in self.raw_data:
                self.errors.add(key, f'Must be specified for {context}',
                                data=self.raw_data)

        self.raw_data['status'] = 'unknown'

        try:
            self._artifact = ArtifactSchema().load(self.raw_data,
                                                   partial=('event_id',))
        except ValidationError as e:
            self.errors.extend(e.messages,
                               data=self.raw_data)

        try:
            self._task = TaskSchema().load(self.raw_data,
                                           partial=('thread_id', 'caller',
                                                    'project', 'run_id'))
        except ValidationError as e:
            self.errors.extend(e.messages,
                               data=self.raw_data)

        if len(self.errors.errors):
            raise MarshalError(self.errors)

    @property
    def artifact(self):
        return self._artifact

    @property
    def task(self):
        return self._task


class ArtifactResource(PiperCiResource):

    def head(self,
             artifact=None,
             task_id=None,
             thread_id=None,
             run_id=None,
             sri=None):

        filter_by = Flagz('accessed_by', 'created_by')
        filter_by.accessed_by = 'accessed_by' in request.args.get('filter', '')
        filter_by.created_by = 'created_by' in request.args.get('filter', '')

        try:
            if artifact:
                art = Artifact.get_by_id(str(artifact))
                return None, 200, {'x-gman-artifact-status': art.status}
            elif task_id:
                arts = self.artifacts_by_task_id(task_id, filter_by)
            elif thread_id:
                arts = self.artifacts_by_thread_id(thread_id, filter_by)
            elif run_id:
                arts = self.artifacts_by_run_id(run_id, filter_by)
            elif sri:
                arts = self.artifacts_by_sri(sri)
            else:
                return None, 400

            return None, 200, {'x-gman-artifacts': len(arts)}

        except (ZeroResults, DoesNotExist):
            return None, 404

    def get(self,
            artifact=None,
            task_id=None,
            thread_id=None,
            run_id=None,
            sri=None):

        filter_by = Flagz('accessed_by', 'created_by')

        filter_by.accessed_by = 'accessed_by' in request.args.get('filter', '')
        filter_by.created_by = 'created_by' in request.args.get('filter', '')

        try:
            if artifact:
                return ArtifactSchema().dump(Artifact.get_by_id(str(artifact)))
            elif sri:
                arts = self.artifacts_by_sri(sri, validate=True)
            elif task_id:
                arts = self.artifacts_by_task_id(task_id, filter_by)
            elif thread_id:
                arts = self.artifacts_by_thread_id(thread_id, filter_by)
            elif run_id:
                arts = self.artifacts_by_run_id(run_id, filter_by)
            else:
                return self.BadRequest()

            return ArtifactSchema(many=True).dump(arts)

        except (ZeroResults, DoesNotExist):
            return self.NotFound()

    def post(self, *args, **kwargs):

        try:
            if len(args) or len(kwargs):
                raise BadRequest('Invalid args specified for POST')

            raw = request.get_json(force=True)
            return ArtifactSchema().dump(self.create_artifact(data=raw))
        except MarshalError as e:
            if 'sri' in e.errors.errors and 'Artifact Exists' in e.errors.errors['sri']:
                return e.errors.emit(), 409
            else:
                return e.errors.emit(), 422
        except DoesNotExist:
            errors = Errors()
            errors.add('task_id', 'does not exist')
            return errors.emit(), 404
        except BadRequest as e:
            return self.BadRequest(str(e))
        except (QueryFailed, Exception) as e:
            return self.InternalError(str(e))

    def create_artifact(self, data):
        marshaller = ArtManMarshaller(data)
        marshaller.enforce('create_artifact')
        task = Task.get(Task.task_id == marshaller.task.task_id)
        try:
            # exact duplicate artifact post detection
            artifact = Artifact.get(
                (Artifact.sri == marshaller.artifact.sri)
                & (Artifact.task_id == marshaller.task.task_id))
            marshaller.errors.add('sri', 'Artifact Exists')
            marshaller.errors.add('event_id', 'Artifact Exists')
            raise MarshalError(marshaller.errors)
        except DoesNotExist:
            # create an event for this artifact and store

            event = gman.TaskResource().create_event(
                task.task_id,
                data={'status': 'info',
                      'message': 'Adding artifact'})

            event = TaskEvent.get(TaskEvent.event_id == event.event_id)

            art_status = marshaller.artifact.status

            artifact = Artifact.create(task=task,
                                       event_id=event,
                                       type=marshaller.artifact.type,
                                       status=art_status,
                                       sri=marshaller.artifact.sri,
                                       uri=marshaller.artifact.uri)

            if not artifact:
                raise QueryFailed(
                    f'Failed to create artifact for task {task.task_id}')
        return artifact
