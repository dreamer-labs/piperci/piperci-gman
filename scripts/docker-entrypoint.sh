#!/bin/sh
set -e

chown -R uwsgi:uwsgi /data
 
# Execute process

exec /build/bin/uwsgi uwsgi.yml
